import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";
let game = reactive({board:Array(9).fill(""), nextPlayer: "O", winner:undefined, roundNumber:1}, "game");

game.getboard = function(i){
  return this.board[i];
}

game.isWinner = function(P){
  const winLines = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
  for(let line of winLines){
    if(game.board[line[0]] === P && game.board[line[1]] === P && game.board[line[2]] === P)
      return true;
  }
  return false;
}

game.play = function(i){
}

game.restart = function(){
}

startReactiveDom();
